import psycopg2
import pickle
import sys
import pandas as pd
from sklearn.model_selection import train_test_split

class colors:
    WHITE = '\x1b[6;30;47m'
    AQUA = '\x1b[6;30;46m'
    GREEN = '\x1b[6;30;42m'
    RED = '\x1b[6;30;41m'
    END = '\x1b[0m'

def train(argvs):
    # 01 load the desired training set from POSTGRES, and then do some data processing
    column_name = ['DEFAULT', 'LIMIT_BAL', 'SEX', 'EDUCATION', 'MARRIAGE', 'AGE', 
                   'PAY_1','PAY_2', 'PAY_3', 'PAY_4', 'PAY_5', 'PAY_6', 'BILL_AMT1', 'BILL_AMT2',
                   'BILL_AMT3', 'BILL_AMT4', 'BILL_AMT5', 'BILL_AMT6', 'PAY_AMT1',
                   'PAY_AMT2', 'PAY_AMT3', 'PAY_AMT4', 'PAY_AMT5', 'PAY_AMT6']

    conn = psycopg2.connect(host='10.249.6.215', port = 5432, user='postgres', password='postgres', database='postgres')

    with conn.cursor() as cur:
        cur.execute(f'''
            SELECT * FROM credit_card_{argvs[1]}
        ''')
        data = cur.fetchall()
    conn.close()
    print(f'{colors.AQUA}[PostgreSQL] credit_card_{argvs[1]} Data loading process completes!{colors.END}')

    data_df = pd.DataFrame.from_records(data, columns=column_name) 

    y = data_df['DEFAULT']
    X = data_df.loc[:,'LIMIT_BAL':'PAY_AMT6']
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.20, random_state = 2021)


    # 02 load the desired model, and then do training for it
    filename = './model.pkl'
    try:
        with open(filename, 'rb') as model_pkl:
            model = pickle.load(model_pkl)
    except FileNotFoundError:
        print('File Not Found.')

    model.fit(X_train, y_train)
    accuracy_train = model.score(X_train, y_train)
    accuracy_test = model.score(X_test, y_test)
    
    
    # 03 export the trained model as a pickle file
    with open('./model.pkl', 'wb') as file:
        pickle.dump(model, file)
        
    print(f'{colors.AQUA}[Sklearn] {argvs[2]}: Model training process completes!{colors.END}')
    print(f'{colors.AQUA}[Sklearn] {argvs[2]}: Accuracy (train) = {accuracy_train:.2%}{colors.END}')
    print(f'{colors.AQUA}[Sklearn] {argvs[2]}: Accuracy (test) = {accuracy_test:.2%}{colors.END}')
    
    
    # 04 export the training results as a CSV file
    tmp = model.predict_proba(X_train)
    y_prob_train = list(tmp.transpose()[1])

    tmp = model.predict_proba(X_test)
    y_prob_test = list(tmp.transpose()[1])

    y_prob = y_prob_train + y_prob_test

    cutoff = 0.5
    
    df = pd.DataFrame.from_dict(
        {
            'model name':['Sklearn Decision Tree'] * len(y), 
            'dataset'   :['Train Set'] * len(y_train) + ['Test Set'] * len(y_test),
            'y_prob'    :y_prob_train + y_prob_test,
            'y_pred'    :['Yes' if i >= cutoff else 'No' for i in y_prob_train] + ['Yes' if i >= cutoff else 'No' for i in y_prob_test],
            'y_true'    :list(y)
        }
    )
    df.to_csv('./model_training_results.csv', index = False, header = True, encoding = 'utf-8')
    

if __name__ == '__main__':
    train(sys.argv)
