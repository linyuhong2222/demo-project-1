import pickle
import pandas as pd
from sklearn.ensemble import GradientBoostingClassifier, RandomForestClassifier
from datetime import datetime

def score():
    # 01 convert an ingoing query into a dataframe
    query = request.form.to_dict()
    query_df = pd.DataFrame(query, index = [0])
    X = query_df.loc[:,'LIMIT_BAL':]
    print(f'{query_df}')

    # 02 load the desired model
    print(f'Active model: {str(active_model)}')

    # 03 do scoring for the dataframe
    y_prob = active_model.predict_proba(X)[0][1]
    y_pred = 'Yes' if y_prob >= 0.5 else 'No'

    if y_prob >= 0.5:
        print(f'Will default: {y_pred} ; Default Probability: {y_prob:.2%}')
    else:
        print(f'Will default: {y_pred} ; Default Probability: {y_prob:.2%}')

    # 04 write the scoring result into a CSV file
    today_datetime = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
    today_date = datetime.now().strftime('%Y%m%d')
    
    with open(f'/fkl/model_output/model_output_{today_date}.csv', 'a') as tmp:
        tmp.write(f'{query_df["CUSTOMER_ID"][0]},{y_prob},{y_pred},{today_datetime}\n')
    
    # 05 return the scoring result to the front-end app
    return f'Will default: {y_pred} ; Default Probability: {y_prob:.2%}'
